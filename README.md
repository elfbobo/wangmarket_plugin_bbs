[wangmarket（网市场云建站系统）](https://gitee.com/mail_osc/wangmarket) 之 论坛插件。让每个网站都有一个自己的、独立的论坛。
![效果图](https://gitee.com/mail_osc/wangmarket_plugin_bbs/raw/master/else/wangmarket_plugin_bbs.png "效果图")

## 使用条件
注意，本项目不能直接运行，需要放到 [网市场云建站系统](https://gitee.com/mail_osc/wangmarket) 中才可运行使用，且网市场云建站系统本身需要 v4.3 或以上版本才可。

## 使用方式
1. 下载
1. 解压，将 /ROOT 文件夹其覆盖到你的应用根目录下，也就是 tomcat/webapps/ROOT/ 下
1. 数据库执行创建论坛相关数据表的语句 /else/mysql.sql
1. 重新启动运行项目，登陆网站管理后台，即可看到左侧的 功能插件 下多了 论坛 功能。


## 二次开发
1. 首先导入主项目，也就是我们网市场云建站系统 https://gitee.com/mail_osc/wangmarket 将此项目导入，能运行起来
1. 下载本项目，将 /src/ 目录下的文件，按照目录结构，粘贴入 网市场云建站系统 的主项目中。
1. 数据库执行创建表语句 /else/mysql.sql
1. 再次运行项目，随便登陆个网站管理后台（默认运行起来后，会自带一个默认账号密码都是 wangzhan 的账号，可以用此账号登陆）
1. 登陆网站管理后台后，找到左侧的 功能插件 下的 论坛，即可。


## 前端模版目录及文件说明
可根据你自己喜好，更改模版的前端展示样式。当前，前端页面都在 src/main/webapp/WEB-INF/view/plugin/bbs/ 目录下

````
├── admin	总管理后台的功能（也就是网站管理后台中的插件）
│   ├── ...略过
├── common	论坛页面通用的一些部分，只是论坛前端的页面，不包括管理
│   ├── foot.jsp	通用尾部
│   ├── head.jsp	通用头部
│   ├── page.jsp	分页
│   └── right.jsp	右侧的最新动态、本周最火的帖子
├── home.jsp	查看别人的个人资料、别人的个人主页
├── login.jsp	登陆页面
├── personal	个人中心相关
│   ├── common	个人中心通用的一些部分
│   │   └── leftMenu.jsp	个人中心通用的，左侧的菜单
│   ├── info.jsp	个人信息设置
│   ├── postCommentList.jsp	我的回帖列表
│   └── postList.jsp	我的发帖列表
├── post	论坛前端帖子相关
│   ├── addPost.jsp	发表帖子
│   ├── commentList.jsp	回帖列表
│   ├── list.jsp	帖子列表
│   └── view.jsp	帖子详情
└── reg.jsp	注册
````

## 交流及求助
交流社区： [bbs.wang.market](http://bbs.leimingyun.com) 求助请到社区论坛发帖，有专人负责解答疑惑<br/>
QQ群：472328584

## 合作洽谈
作者：管雷鸣<br/>
微信：xnx3com<br/>
QQ：921153866<br/>


## 插件开发说明
|||
| ------------- |-------------|
| 管理后台插件的开发及示例 | [iw.xnx3.com/5902.html](http://iw.xnx3.com/5902.html) |
| 网站访问插件的开发及示例 | [iw.xnx3.com/5818.html](http://iw.xnx3.com/5818.html) |

