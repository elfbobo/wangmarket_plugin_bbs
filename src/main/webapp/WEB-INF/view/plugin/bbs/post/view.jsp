<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>

<jsp:include page="../common/head.jsp">
	<jsp:param name="title" value="${postVO.post.title }"/>
	<jsp:param name="keywords" value="${postVO.post.title }"/>
	<jsp:param name="description" value="${postVO.post.info }"/>
</jsp:include>
<script type="text/javascript" src="${AttachmentFileNetUrl }site/${postVO.post.siteid }/data/plugin_bbs_postClass.js"></script>
<script type="text/javascript" src="//res.weiunity.com/js/xss.js"></script>


<div class="layui-hide-xs">
  <div class="fly-panel fly-column">
    <div class="layui-container">
      <ul class="layui-clear">
        <li class="layui-hide-xs"><a href="list.do?siteid=${postVO.post.siteid }">所有帖子</a></li>
<script>
try{
	if(typeof(postClass) == 'undefined'){
		var postClass = new Array();
		postClass['0'] = '请在管理后台添加论坛板块';
	}
	for (var p in postClass) {
        document.write('<li id="class_'+p+'"><a href="list.do?siteid=${postVO.post.siteid }&classid='+p+'">'+postClass[p]+'</a></li>');
    }
    document.getElementById('class_${postVO.post.classid }').setAttribute("class", "layui-this");
}catch(e){
	console.log(e);
}
</script>        
        
        <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><span class="fly-mid"></span></li> 
        
      </ul> 
    </div>
  </div>
</div>

<div class="layui-container">
  <div class="layui-row layui-col-space15">
    <div class="layui-col-md8 content detail">
      <div class="fly-panel detail-box">
        <h1>${postVO.post.title }</h1>
        <div class="fly-detail-info" style=" position: inherit; height: 0px; line-height: 90px; margin-right: 30px;">
          
          <span class="layui-badge"></span>
          
          
          <span class="fly-list-nums" style="margin-right: 30px;"> 
            <a href="#comment"><i class="iconfont" title="回复">&#xe60c;</i> ${postVO.post.commentCount }</a>
            <i class="iconfont" title="人气">&#xe60b;</i> ${postVO.post.view }
          </span>
        </div>
        <div class="detail-about">
          <a class="fly-avatar" href="home.do?siteid=${postVO.post.siteid }&userid=${postVO.user.id }" style="top: 8px;">
            <img src="${postVO.user.head }" alt="${postVO.user.nickname }" onerror="this.src='//res.weiunity.com/image/default_head.png'" style="width: 35px; height: 35px; border-radius: 35px;" />
          </a>
          <div class="fly-detail-user">
            <a href="home.do?siteid=${postVO.post.siteid }&userid=${postVO.user.id }" class="fly-link">
              <cite>${postVO.user.nickname }</cite>
            </a>
            <span><x:time linuxTime="${postVO.post.addtime }"></x:time></span>
          </div>
        </div>
        <div class="detail-body photos" id="text">
        	<!-- url编码的内容 -->
        	<textarea id="urltext" style="display:none;">${utf8text }</textarea>
          	加载中...
        </div>
      </div>

      <div class="fly-panel detail-box" id="flyReply">
      <a name="comment"></a>
        <fieldset class="layui-elem-field layui-field-title" style="text-align: center;">
          <legend>回帖</legend>
        </fieldset>
        <style>
        .laypage-main * {
       	    padding: 0 11px;
   			line-height: 25px;
        }
        </style>
        <ul class="jieda" id="jieda">
        	<li class="fly-none">加载中...</li>
        </ul>
        
        <div id="apply_login_panel" class="layui-form layui-form-pane" style="border-style: solid; border-color: #e6e6e6;border-width: 1px;padding: 50px;">
	        <div class="layui-form-item" style="text-align:center;">
	        	<div style="color: gray; padding-bottom: 20px;">您需要登陆之后后才能进行回复～～～～～</div>
	        	<a href="login.do?siteid=${postVO.post.siteid }&classid=${postVO.post.classid }&postid=${postVO.post.id }" class="layui-btn layui-btn-lg" style="margin-right:20px;">立即登陆</a>
	        	<a href="javascript:regJump();" class="layui-btn layui-btn-primary  layui-btn-lg"  style="margin-left:20px;">免费注册</a>
	        </div>
        </div>
        
        <!-- 默认先让回复的这个隐藏 -->
        <div id="apply_form_panel" style="display:none;" class="layui-form layui-form-pane">
          <form action="" method="post">
          	<input type="hidden" name="postid" value="${postVO.post.id }" />
            <div class="layui-form-item layui-form-text">
              <div class="layui-input-block">
                <textarea id="text" name="text" required lay-verify="required" placeholder="请输入2000字以内的回复内容"  class="layui-textarea fly-editor" style="height: 150px;"></textarea>
              </div>
            </div>
            
            <div class="layui-form-item">
                <label for="L_vercode" class="layui-form-label">验证</label>
                <div class="layui-input-inline">
                  <input type="text" id="code" name="code" required lay-verify="required" placeholder="请输入右边的验证码" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid yzm">
                  <span style="color: #c00;">
                  	<img id="codeImg" src="/captcha.do" onclick="reloadCode();" style="height: 22px;width: 110px; cursor: pointer;"/>
                  </span>
                </div>
                
                <button class="layui-btn" lay-submit="" lay-filter="demo1" style="margin-left:20px;">立即发布</button>
              </div>
            
          </form>
        </div>
        
        <script>
        	if('${user.id}' > 0){
        		//已登陆
        		document.getElementById('apply_form_panel').style.display='block';
        		document.getElementById('apply_login_panel').style.display='none';
        	}else{
				//未登陆
			}
        </script>
        
        
      </div>
    </div>
    <div class="layui-col-md4">
      <jsp:include page="../common/right.jsp"></jsp:include> 
    </div>
  </div>
</div>





<script type="text/javascript">
//加载帖子内容
layui.use('util', function(){
  var util = layui.util;
  var urltext = unescape(document.getElementById('urltext').value);
  document.getElementById('text').innerHTML = filterXSS(unescape(urltext.replace(/\\/g,'%'))); 
});

//时间戳转时间
function formatDateTime(timeStamp) { 
    var date = new Date();
    date.setTime(timeStamp * 1000);
    var y = date.getFullYear();    
    var m = date.getMonth() + 1;    
    m = m < 10 ? ('0' + m) : m;    
    var d = date.getDate();    
    d = d < 10 ? ('0' + d) : d;    
    var h = date.getHours();  
    h = h < 10 ? ('0' + h) : h;  
    var minute = date.getMinutes();  
    var second = date.getSeconds();  
    minute = minute < 10 ? ('0' + minute) : minute;    
    second = second < 10 ? ('0' + second) : second;   
    return y + '-' + m + '-' + d + '&nbsp;'+h+':'+minute;    
}; 

//获取生成的分页的html. data便是获取到的接口的json对象
function getPage(data){
	var siteid = '${siteid}';
	var postid = '${postid}';
	var page = data.page;
	var pageHtml = '<div id="bbs_page" style="text-align: center;"><div class="laypage-main">';
	//首页
	if(!page.currentFirstPage){
		pageHtml = pageHtml + '<a href="javascript:loadPostComment(1);">第一页</a><a href="javascript:loadPostComment('+page.upPageNumber+');">上一页</a>';
	}
	// 上几页
	if(page.upList.length > 0){
		for(var ul=0,uls=page.upList.length;ul<uls;ul++){
			pageHtml = pageHtml + '<a href="javascript:loadPostComment('+page.upList[ul]['pageNumber']+');">'+page.upList[ul]['title']+'</a>';
		}
	}
	//当前页面
	pageHtml = pageHtml + '<span class="laypage-curr">'+page.currentPageNumber+'</span>';
	//下几页
	if(page.nextList.length > 0){
		for(var nl=0,nls=page.nextList.length;nl<nls;nl++){
			pageHtml = pageHtml + '<a href="javascript:loadPostComment('+page.nextList[nl]['pageNumber']+');">'+page.nextList[nl]['title']+'</a>';
		}
	}
	//尾页
	if(!page.currentLastPage){
		pageHtml = pageHtml + '<a href="javascript:loadPostComment('+page.nextPageNumber+');">下一页</a><a href="javascript:loadPostComment('+page.lastPageNumber+');">最后一页</a>';
	}
	pageHtml = pageHtml + '<span>共'+page.allRecordNumber+'条，'+page.currentPageNumber+'/'+page.lastPageNumber+'页</span>';
	
	pageHtml = pageHtml + '</div></div>';
	return pageHtml;
}


//加载回帖.  pageNumber要加载第几页
function loadPostComment(pageNumber){
	document.getElementById('jieda').innerHTML = '<li class="fly-none">加载中...</li>';
	$.post("/plugin/bbs/postCommentList.do?siteid=${postVO.post.siteid }&postid=${postVO.post.id }&currentPage="+pageNumber, function(data){
	    var html = '';
	    if(data.result == '1'){
	    	if(data.list.length == 0){
	    		html = html + '<li class="fly-none">暂无回复</li>';
	    	}else{
				for(var i=0,l=data.list.length;i<l;i++){
		        	postComment = data.list[i];
		        	html = html + '<li class="jieda-daan"><a class="fly-avatar" href="home.do?siteid='+postComment['siteid']+'&userid='+postComment['userid']+'" style="top:20px;"><img src="'+postComment['head']+'" alt="'+postComment['nickname']+'" onerror="this.src=\'//res.weiunity.com/image/default_head.png\'" style="width:20px; height: 20px;border-radius: 10px;" /></a><div class="detail-about detail-about-reply"><div class="fly-detail-user"><a href="home.do?siteid='+postComment['siteid']+'&userid='+postComment['userid']+'" class="fly-link"><cite style="padding-right: 10px;">'+postComment['nickname']+'</cite>回复于 '+formatDateTime(postComment['addtime'])+'</a></div></div><div class="detail-body jieda-body photos">'+filterXSS(postComment['text'])+'</div></li>';
		        }
		        
		        html = html + getPage(data);
	    	}
	    }else if(data.result == '0'){
	    	html = html + '<li class="fly-none">'+data.info+'</li>';
	    }else{
	    	html = html + '<li class="fly-none">获取失败</li>';
	    }
	    
	    document.getElementById('jieda').innerHTML = html;
	});
}
loadPostComment(1);



//重新加载验证码
function reloadCode(){
	var code=document.getElementById('codeImg');
	code.setAttribute('src','/captcha.do?'+Math.random());
	//这里必须加入随机数不然地址相同我发重新加载
}

layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form;
  //监听提交
	form.on('submit(demo1)', function(data){
		iw.loading('保存中');
		var d=$("form").serialize();
        $.post("addCommentSubmit.do", d, function (result) { 
        	iw.loadClose();
        	var obj = JSON.parse(result);
        	if(obj.result == '1'){
        		iw.msgSuccess("发布成功");
        		window.location.reload();
        	}else if(obj.result == '0'){
        		reloadCode();
        		layer.msg(obj.info, {shade: 0.3})
        	}else{
        		reloadCode();
        		layer.msg(result, {shade: 0.3})
        	}
         }, "text");
		
		return false;
  });
  
});
</script>




<jsp:include page="../common/foot.jsp"></jsp:include>