package com.xnx3.wangmarket.plugin.bbs.vo;

import java.util.List;

import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;

/**
 * 帖子列表
 * @author 管雷鸣
 *
 */
public class PostListVO extends BaseVO{
	List<Post> list;
	Page page;

	public List<Post> getList() {
		return list;
	}

	public void setList(List<Post> list) {
		this.list = list;
	}

	
	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	@Override
	public String toString() {
		return "PostListVO [list=" + list + "]";
	}
	
	
}
