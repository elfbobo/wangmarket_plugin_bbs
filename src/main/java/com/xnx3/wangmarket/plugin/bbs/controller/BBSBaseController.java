package com.xnx3.wangmarket.plugin.bbs.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;
import com.xnx3.wangmarket.plugin.bbs.Global;

/**
 * 论坛插件的 controller 继承此
 * @author 管雷鸣
 *
 */
public class BBSBaseController extends BasePluginController{
	
	/**
	 * 设置 附件域名、user 等
	 * @param model
	 */
	public void modelSet(HttpServletRequest request, Model model){
		model.addAttribute("user", getUser());
		model.addAttribute("AttachmentFileNetUrl", AttachmentFile.netUrl());
		model.addAttribute("siteid", getRequestSiteid(request));
		model.addAttribute("classid", getRequestClassid(request));
		model.addAttribute("postid", getRequestPostid(request));
		model.addAttribute("ATTACHMENT_JSON_FILE_NAME", Global.ATTACHMENT_JSON_FILE_NAME);
	}
	

	/**
	 * 获取当前get或post传递的siteid参数，若不存在，则返回空字符串
	 * @param request
	 * @return
	 */
	public String getRequestSiteid(HttpServletRequest request){
		String siteid = getRequestParam(request, "siteid");
		return siteid;
	}
	
	
	/**
	 * 获取当前get或post传递的classid参数，若不存在，则返回空字符串
	 * @param request
	 * @return
	 */
	public String getRequestClassid(HttpServletRequest request){
		return getRequestParam(request, "classid");
	}
	
	/**
	 * 获取当前get或post传递的postid参数，若不存在，则返回空字符串
	 * @param request
	 * @return
	 */
	public String getRequestPostid(HttpServletRequest request){
		return getRequestParam(request, "postid");
	}
	
}
