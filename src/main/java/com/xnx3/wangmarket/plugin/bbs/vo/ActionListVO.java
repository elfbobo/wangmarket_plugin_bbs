package com.xnx3.wangmarket.plugin.bbs.vo;

import java.util.List;

import com.xnx3.j2ee.vo.BaseVO;

/**
 * 最新动作列表
 * @author 管雷鸣
 * 
 */
public class ActionListVO extends BaseVO{
	
	//动作列表，其内有每条动作。动作记录支持html
	private List<String> list;

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}
	
	
	
}
